const express = require('express');
const app = express();
const router = express.Router();
const port = 3000;
const url = require('url');

// all routes prefixed with /api
app.use('/api', router);

// using router.get() to prefix our path
// url: http://localhost:3000/api/
router.get('/', (request, response) => {
  response.json({message: 'Hello, welcome to my server'});
});

router.get('/temp/map', (request, response) => {
    response.json([{"id":1, "name":"Akácos út", "long":47.551273, "lat": 18.824111, "batt":34, "temp":25.8}, 
    			   {"id":2, "name":"Barka utca", "long":47.555568, "lat": 18.821866, "batt":34, "temp":26.3},
    			   {"id":3, "name":"Anna-Laki út", "long":47.553408, "lat": 18.830218, "batt":34, "temp":26.6},
    			   {"id":4, "name":"Cinege utca", "long":47.539075, "lat": 18.823553, "batt":34, "temp":27.1},
    			   {"id":5, "name":"Napsugár út", "long":47.548258, "lat": 18.819023, "batt":34, "temp":26.8}	
    			   ]);
});

router.get('/temp/sensor', (request, response) => {
    var urlParts = url.parse(request.url, true);
    var parameters = urlParts.query;
    var sensorId = parameters.sensorId;
  
    var res;

    if(sensorId == 1) {
        res = {"id":1, "long":40.412124, "lat": 45.234234, "batt":34, "temps":[{"2018-03-26-00-01":5}, {"2018-03-26-05-01":10}, {"2018-03-26-10-01":15}, {"2018-03-26-15-01":20}]};
    } else {
        res = {"id":2, "long":40.412124, "lat": 45.234234, "batt":34, "temps":[{"2018-03-26-00-01":5}, {"2018-03-26-05-01":10}, {"2018-03-26-10-01":15}, {"2018-03-26-15-01":20}]};
    }
  
    response.json(res);
});

router.get('/parking/times', (request, response) => {
    response.json([{"bucket": "1-5 perc", "value": 7},
                   {"bucket": "5-10 perc", "value": 2},
                   {"bucket": "10-20 perc", "value": 1},
                   {"bucket": "20-30 perc", "value": 2},
                   {"bucket": "30 perc felett", "value": 1}
                  ])
});

router.get('/parking/status', (request, response) => {
    response.json({"free": 6, "occupied": 3, "disabledFree": true})
});


router.get('/parking/average', (request, response) => {
    response.json([{"day": "Hétfő", "id": 0, "value": 7},
                   {"day": "Kedd", "id": 1, "value": 8},
                   {"day": "Szerda", "id": 2, "value": 7},
                   {"day": "Csütörtök", "id": 3, "value": 9},
                   {"day": "Péntek", "id": 4, "value": 6},
                   {"day": "Szombat", "id": 5, "value": 4},
                   {"day": "Vasárnap", "id": 6, "value": 2}
                  ])
});


router.get('/bus/schedule', (request, response) => {
    var urlParts = url.parse(request.url, true);
    var parameters = urlParts.query;
    var busId = parameters.busId;
  
    var res;

    if(busId == 1) {
        res = {"id":1, "arrivals":["05-00", "06-10", "07-20", "8-30", "9-40", "10-50", "11-50", "13-00", "14-10"]};
    } else {
        res = {"id":2, "arrivals":["05-00", "06-00", "07-00", "8-00", "9-00", "10-00", "11-00", "13-00", "14-00"]};
    }

    response.json(res);
});

router.get('/bus/lastseen', (request, response) => {
    var urlParts = url.parse(request.url, true);
    var parameters = urlParts.query;
    var busId = parameters.busId;
  
    var res;

    if(busId == 1) {
        res = {"id":1, "seen":["05-02", "06-13", "07-20", "8-31", "9-39", "10-52", "11-50", "13-02", "14-08"]};
    } else {
        res = {"id":2, "seen":["05-01", "06-02", "07-00", "8-01", "9-05", "10-01", "11-00", "13-02", "14-03"]};
    }

    response.json(res);
});

// set the server to listen on port 3000
app.listen(port, () => console.log(`Listening on port ${port}`));